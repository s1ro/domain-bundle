<?php

namespace Domain\Service;

use LayerShifter\TLDExtract\Extract;

class TldExtractor
{
    /**
     * @param string $domain
     * @return string
     */
    public static function extractTld(string $domain): ?string
    {
        $extract = new Extract();
        return $extract->parse($domain)->getSuffix();
    }
}