<?php

namespace Domain\Service\Godaddy\Model;

class GodaddyDnsRecord implements \JsonSerializable
{
    /**
     * @var string
     */
    private $data;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $type;
    /**
     * @var int|null
     */
    private $port;
    /**
     * @var int|null
     */
    private $priority;
    /**
     * @var string|null
     */
    private $protocol;
    /**
     * @var string|null
     */
    private $service;
    /**
     * @var int|null
     */
    private $ttl;
    /**
     * @var int|null
     */
    private $weight;

    public function __construct(string $data, string $name, string $type)
    {
        $this->data = $data;
        $this->name = $name;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int|null
     */
    public function getPort(): ?int
    {
        return $this->port;
    }

    /**
     * @param int|null $port
     */
    public function setPort(?int $port): void
    {
        $this->port = $port;
    }

    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * @param int|null $priority
     */
    public function setPriority(?int $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return string|null
     */
    public function getProtocol(): ?string
    {
        return $this->protocol;
    }

    /**
     * @param string|null $protocol
     */
    public function setProtocol(?string $protocol): void
    {
        $this->protocol = $protocol;
    }

    /**
     * @return string|null
     */
    public function getService(): ?string
    {
        return $this->service;
    }

    /**
     * @param string|null $service
     */
    public function setService(?string $service): void
    {
        $this->service = $service;
    }

    /**
     * @return int|null
     */
    public function getTtl(): ?int
    {
        return $this->ttl;
    }

    /**
     * @param int|null $ttl
     */
    public function setTtl(?int $ttl): void
    {
        $this->ttl = $ttl;
    }

    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int|null $weight
     */
    public function setWeight(?int $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        $result = [
            'data' => $this->data,
            'name' => $this->name,
            'type' => $this->type
        ];
        if (!is_null($this->port)) {
            $result['port'] = $this->port;
        }
        if (!is_null($this->priority)) {
            $result['priority'] = $this->priority;
        }
        if (!is_null($this->protocol)) {
            $result['protocol'] = $this->protocol;
        }
        if (!is_null($this->service)) {
            $result['service'] = $this->service;
        }
        if (!is_null($this->ttl)) {
            $result['ttl'] = $this->ttl;
        }
        if (!is_null($this->weight)) {
            $result['weight'] = $this->weight;
        }
        return $result;
    }
}