<?php

namespace Domain\Service\Godaddy\Model;

class GodaddyDomainPurchaseResponse
{
    /**
     * @var int
     */
    private $itemCount;
    /**
     * @var int
     */
    private $orderId;
    /**
     * @var int
     */
    private $total;
    /**
     * @var string|null
     */
    private $currency;

    public function __construct(int $itemCount, int $orderId, int $total, ?string $currency)
    {
        $this->itemCount = $itemCount;
        $this->orderId = $orderId;
        $this->total = $total;
        $this->currency = $currency;
    }

    /**
     * @return int
     */
    public function getItemCount(): int
    {
        return $this->itemCount;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }
}