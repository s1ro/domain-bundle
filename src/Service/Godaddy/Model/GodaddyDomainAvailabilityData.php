<?php

namespace Domain\Service\Godaddy\Model;

class GodaddyDomainAvailabilityData
{
    /**
     * @var bool
     */
    private $available;
    /**
     * @var bool
     */
    private $definitive;
    /**
     * @var string
     */
    private $domain;
    /**
     * @var int|null
     *
     */
    private $period;
    /**
     * @var int|null
     */
    private $price;
    /**
     * @var string|null
     */
    private $currency;

    public function __construct(
        bool $available,
        bool $definitive,
        string $domain,
        ?int $period,
        ?int $price,
        ?string $currency
    )
    {
        $this->available = $available;
        $this->definitive = $definitive;
        $this->domain = $domain;
        $this->period = $period;
        $this->price = $price;
        $this->currency = $currency;
    }

    /**
     * @return bool
     */
    public function getAvailable(): bool
    {
        return $this->available;
    }

    /**
     * @return bool
     */
    public function getDefinitive(): bool
    {
        return $this->definitive;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @return int|null
     */
    public function getPeriod(): ?int
    {
        return $this->period;
    }

    /**
     * @return int|null
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }
}