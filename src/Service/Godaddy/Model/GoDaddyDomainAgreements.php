<?php

namespace Domain\Service\Godaddy\Model;

class GoDaddyDomainAgreements
{
    /**
     * @var string
     */
    private $agreementKey;
    /**
     * @var string
     */
    private $content;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string|null
     */
    private $url;

    public function __construct(string $agreementKey, string $content, string $title, ?string $url)
    {
        $this->agreementKey = $agreementKey;
        $this->content = $content;
        $this->title = $title;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getAgreementKey(): string
    {
        return $this->agreementKey;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }
}