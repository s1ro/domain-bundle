<?php

namespace Domain\Service\Godaddy;

class GodaddyContactInfoProvider
{
    /**
     * @var array
     */
    private $contactInfo;

    public function __construct(array $contactInfo)
    {
        $this->contactInfo = $contactInfo;
    }

    /**
     * @return array
     */
    public function getContactInfo(): array
    {
        return $this->contactInfo;
    }
}