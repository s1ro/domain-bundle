<?php

namespace Domain\Service\Godaddy;

use Domain\Service\TestService;

class GodaddyKeysProvider
{
    /**
     * @var string
     */
    private $prodPublicKey;
    /**
     * @var string
     */
    private $prodPrivateKey;
    /**
     * @var string
     */
    private $devPublicKey;
    /**
     * @var string
     */
    private $devPrivateKey;
    /**
     * @var TestService
     */
    private $testService;

    public function __construct(
        string $prodPublicKey,
        string $prodPrivateKey,
        string $devPublicKey,
        string $devPrivateKey,
        TestService $testService
    )
    {
        $this->prodPublicKey = $prodPublicKey;
        $this->prodPrivateKey = $prodPrivateKey;
        $this->devPublicKey = $devPublicKey;
        $this->devPrivateKey = $devPrivateKey;
        $this->testService = $testService;
    }

    /**
     * @return string
     */
    public function getPublicKey(): string
    {
        if ($this->testService->isTest()) {
            return $this->devPublicKey;
        }
        return $this->prodPublicKey;
    }

    /**
     * @return string
     */
    public function getPrivateKey(): string
    {
        if ($this->testService->isTest()) {
            return $this->devPrivateKey;
        }
        return $this->devPublicKey;
    }

    /**
     * @return string
     */
    public function getAuthRow(): string
    {
        if ($this->testService->isTest()) {
            return $this->devPublicKey . ':' . $this->devPrivateKey;
        }
        return $this->prodPublicKey . ':' . $this->prodPrivateKey;
    }
}