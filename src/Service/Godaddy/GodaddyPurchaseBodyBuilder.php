<?php

namespace Domain\Service\Godaddy;

use Domain\Service\Godaddy\Exception\GodaddyException;
use Domain\Service\TldExtractor;
use LayerShifter\TLDExtract\Extract;
use Symfony\Component\HttpFoundation\Response;

class GodaddyPurchaseBodyBuilder
{
    /**
     * @var GodaddyClient
     */
    private $godaddyClient;
    /**
     * @var GodaddyContactInfoProvider
     */
    private $godaddyContactInfoProvider;

    public function __construct(GodaddyClient $godaddyClient, GodaddyContactInfoProvider $godaddyContactInfoProvider)
    {
        $this->godaddyClient = $godaddyClient;
        $this->godaddyContactInfoProvider = $godaddyContactInfoProvider;
    }

    /**
     * @param string $domain
     * @return array
     * @throws Exception\GodaddyClientException
     * @throws GodaddyException
     */
    public function build(string $domain): array
    {
        $consent = $this->buildConsent($domain);
        $contact = $this->godaddyContactInfoProvider->getContactInfo();
        return [
            'consent' => $consent,
            'contactAdmin' => $contact,
            'contactBilling' => $contact,
            'contactRegistrant' => $contact,
            'contactTech' => $contact,
            'domain' => $domain,
            'nameServers' => [
                "NS05.DOMAINCONTROL.COM",
                "NS06.DOMAINCONTROL.COM"
            ],
            'period' => 1,
            'privacy' => false,
            'renewAuto' => false,
        ];
    }

    /**
     * @param string $domain
     * @return array
     * @throws GodaddyException
     * @throws Exception\GodaddyClientException
     */
    private function buildConsent(string $domain): array
    {
        $tld = TldExtractor::extractTld($domain);
        if (!$tld) {
            throw new GodaddyException('Invalid domain', Response::HTTP_BAD_REQUEST);
        }
        $domainAgreements = $this->godaddyClient->getDomainAgreements([$tld], false);
        $keys = [];
        foreach ($domainAgreements as $domainAgreement) {
            $keys[] = $domainAgreement->getAgreementKey();
        }
        $keys = array_unique($keys);
        return [
            'agreedAt' => (new \DateTime('now'))->format('Y-m-d\TH:i:s') . 'Z',
            'agreedBy' => gethostbyname(gethostname()),
            'agreementKeys' => $keys,
        ];
    }
}