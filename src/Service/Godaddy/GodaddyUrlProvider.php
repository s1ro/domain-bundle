<?php

namespace Domain\Service\Godaddy;

use Domain\Service\TestService;

class GodaddyUrlProvider
{
    private const DEV_URL = 'https://api.ote-godaddy.com';
    private const PROD_URL = 'https://api.godaddy.com';

    /**
     * @var TestService
     */
    private $testService;

    public function __construct(TestService $testService)
    {
        $this->testService = $testService;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        if ($this->testService->isTest()) {
            return self::DEV_URL;
        }
        return self::PROD_URL;
    }
}