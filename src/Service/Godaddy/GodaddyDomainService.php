<?php

namespace Domain\Service\Godaddy;

use Domain\Service\Godaddy\Model\GodaddyDnsRecord;
use Domain\Service\Godaddy\Model\GodaddyDomainAvailabilityData;
use Domain\Service\UserDomainRelationService;

class GodaddyDomainService
{
    /**
     * @var GodaddyClient
     */
    private $godaddyClient;
    /**
     * @var GodaddyPurchaseBodyBuilder
     */
    private $godaddyPurchaseBodyBuilder;
    /**
     * @var UserDomainRelationService
     */
    private $userDomainRelationService;

    public function __construct(
        GodaddyClient $godaddyClient,
        GodaddyPurchaseBodyBuilder $godaddyPurchaseBodyBuilder,
        UserDomainRelationService $userDomainRelationService
    )
    {
        $this->godaddyClient = $godaddyClient;
        $this->godaddyPurchaseBodyBuilder = $godaddyPurchaseBodyBuilder;
        $this->userDomainRelationService = $userDomainRelationService;
    }

    /**
     * @param string $domain
     * @param string $checkType
     * @param bool $forTransfer
     * @return GodaddyDomainAvailabilityData
     * @throws Exception\GodaddyClientException
     */
    public function checkDomainAvailability(
        string $domain,
        string $checkType = GodaddyClient::AVAILABILITY_CHECK_TYPE_FAST,
        bool $forTransfer = false
    ): GodaddyDomainAvailabilityData
    {
        return $this->godaddyClient->checkDomainAvailability($domain, $checkType, $forTransfer);
    }

    /**
     * @param string $query
     * @param string|null $country
     * @param string|null $city
     * @param array|null $sources
     * @param array|null $tlds
     * @param int|null $lengthMax
     * @param int|null $lengthMin
     * @param int|null $limit
     * @param int $waitMs
     * @return string[]
     * @throws Exception\GodaddyClientException
     */
    public function getDomainSuggestions(
        string $query,
        ?string $country = null,
        ?string $city = null,
        ?array $sources = null,
        ?array $tlds = null,
        ?int $lengthMax = null,
        ?int $lengthMin = null,
        ?int $limit = null,
        int $waitMs = GodaddyClient::SUGGEST_DEFAULT_WAIT_MS
    ): array
    {
        return $this->godaddyClient->getDomainSuggestions(
            $query, $country, $city, $sources, $tlds, $lengthMax, $lengthMin, $limit, $waitMs
        );
    }

    /**
     * @param string $domain
     * @param string $userIdentifier
     * @return bool
     * @throws Exception\GodaddyException
     * @throws Exception\GodaddyClientException
     */
    public function purchaseDomain(string $domain, string $userIdentifier): bool
    {
        $purchaseRequestBody = $this->godaddyPurchaseBodyBuilder->build($domain);
        $this->godaddyClient->purchaseDomain($purchaseRequestBody);
        $this->userDomainRelationService->addDomainToUser($userIdentifier, $domain);
        return true;
    }

    /**
     * @param string $domain
     * @param GodaddyDnsRecord[] $records
     * @return bool
     * @throws Exception\GodaddyClientException
     */
    public function replaceDnsRecords(string $domain, array $records): bool
    {
        return $this->godaddyClient->replaceDnsRecords($domain, $records);
    }
}