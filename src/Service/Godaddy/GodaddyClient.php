<?php

namespace Domain\Service\Godaddy;

use Domain\Service\Godaddy\Builder\GodaddyDomainAgreementsBuilder;
use Domain\Service\Godaddy\Builder\GodaddyDomainAvailabilityDataBuilder;
use Domain\Service\Godaddy\Builder\GodaddyDomainPurchaseResponseBuilder;
use Domain\Service\Godaddy\Exception\GodaddyClientException;
use Domain\Service\Godaddy\Model\GodaddyDnsRecord;
use Domain\Service\Godaddy\Model\GoDaddyDomainAgreements;
use Domain\Service\Godaddy\Model\GodaddyDomainAvailabilityData;
use Domain\Service\Godaddy\Model\GodaddyDomainPurchaseResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class GodaddyClient
{
    public const AVAILABILITY_CHECK_TYPE_FULL = 'FULL';
    public const AVAILABILITY_CHECK_TYPE_FAST = 'FAST';
    public const AVAILABILITY_CHECK_TYPES = [
        self::AVAILABILITY_CHECK_TYPE_FAST, self::AVAILABILITY_CHECK_TYPE_FULL, 'fast', 'full'
    ];

    public const SUGGEST_SOURCE_CC_TLD = 'CC_TLD';
    public const SUGGEST_SOURCE_EXTENSION = 'EXTENSION';
    public const SUGGEST_SOURCE_KEYWORD_SPIN = 'KEYWORD_SPIN';
    public const SUGGEST_SOURCE_PREMIUM = 'PREMIUM';
    public const SUGGEST_SOURCES = [
        self::SUGGEST_SOURCE_CC_TLD,
        self::SUGGEST_SOURCE_EXTENSION,
        self::SUGGEST_SOURCE_KEYWORD_SPIN,
        self::SUGGEST_SOURCE_PREMIUM,
        'cctld', 'extension', 'keywordspin', 'premium'
    ];
    public const SUGGEST_DEFAULT_WAIT_MS = 1000;

    /**
     * @var GodaddyKeysProvider
     */
    private $godaddyKeysProvider;
    /**
     * @var GodaddyUrlProvider
     */
    private $godaddyUrlProvider;
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    public function __construct(
        GodaddyKeysProvider $godaddyKeysProvider,
        GodaddyUrlProvider $godaddyUrlProvider,
        HttpClientInterface $httpClient
    )
    {
        $this->godaddyKeysProvider = $godaddyKeysProvider;
        $this->godaddyUrlProvider = $godaddyUrlProvider;
        $this->httpClient = $httpClient;
    }

    /**
     * @param ResponseInterface $response
     * @return array
     * @throws GodaddyClientException
     */
    private function prepareData(ResponseInterface $response): array
    {
        try {
            $data = json_decode($response->getContent(), true);
            if (is_null($data)) {
                throw new GodaddyClientException('Json decode error!', Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            if (isset($data['code'], $data['message'])) {
                throw new GodaddyClientException(
                    $data['code'] . ': ' . $data['message'],
                    $response->getStatusCode()
                );
            }
        } catch (ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface | TransportExceptionInterface $e) {
            throw new GodaddyClientException($e->getMessage());
        }
        return $data;
    }

    /**
     * @param string $domain
     * @param string $checkType
     * @param bool $forTransfer
     * @return GodaddyDomainAvailabilityData
     * @throws GodaddyClientException
     */
    public function checkDomainAvailability(
        string $domain,
        string $checkType = self::AVAILABILITY_CHECK_TYPE_FAST,
        bool $forTransfer = false
    ): GodaddyDomainAvailabilityData
    {
        if (!in_array($checkType, self::AVAILABILITY_CHECK_TYPES, true)) {
            throw new GodaddyClientException('Invalid checkType', Response::HTTP_BAD_REQUEST);
        }
        $url = $this->godaddyUrlProvider->getUrl();
        $url .= '/v1/domains/available';
        try {
            $response = $this->httpClient->request('GET', $url, [
                'headers' => [
                    'Authorization' => 'sso-key ' . $this->godaddyKeysProvider->getAuthRow(),
                ],
                'query' => [
                    'domain' => $domain,
                    'checkType' => $checkType,
                    'forTransfer' => $forTransfer
                ],
            ]);
            $data = $this->prepareData($response);
            if (!isset($data['available'], $data['definitive'], $data['domain'])) {
                throw new GodaddyClientException(
                    'Unexpected response from Godaddy',
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }
            return GodaddyDomainAvailabilityDataBuilder::build($data);
        } catch (TransportExceptionInterface $e) {
            throw new GodaddyClientException($e->getMessage());
        }
    }

    /**
     * @param string $query
     * @param string|null $country
     * @param string|null $city
     * @param string[]|null $sources
     * @param string[]|null $tlds
     * @param int|null $lengthMax
     * @param int|null $lengthMin
     * @param int|null $limit
     * @param int $waitMs
     * @return string[]
     * @throws GodaddyClientException
     */
    public function getDomainSuggestions(
        string $query,
        ?string $country = null,
        ?string $city = null,
        ?array $sources = null,
        ?array $tlds = null,
        ?int $lengthMax = null,
        ?int $lengthMin = null,
        ?int $limit = null,
        int $waitMs = self::SUGGEST_DEFAULT_WAIT_MS
    ): array
    {
        if (!is_null($sources)) {
            foreach ($sources as $source) {
                if (!in_array($source, self::SUGGEST_SOURCES, true)) {
                    throw new GodaddyClientException(
                        'Invalid source ' . $source,
                        Response::HTTP_BAD_REQUEST
                    );
                }
            }
        }
        $url = $this->godaddyUrlProvider->getUrl();
        $url .= '/v1/domains/suggest';
        $requestQuery = ['query' => $query, 'waitMs' => $waitMs];
        if (!is_null($country)) {
            $requestQuery['country'] = $country;
        }
        if (!is_null($city)) {
            $requestQuery['city'] = $city;
        }
        if (!empty($sources)) {
            $requestQuery['sources'] = $sources;
        }
        if (!empty($tlds)) {
            $requestQuery['tlds'] = $tlds;
        }
        if (!is_null($lengthMax)) {
            $requestQuery['lengthMax'] = $lengthMax;
        }
        if (!is_null($lengthMin)) {
            $requestQuery['lengthMin'] = $lengthMin;
        }
        if (!is_null($limit)) {
            $requestQuery['limit'] = $limit;
        }
        try {
            $response = $this->httpClient->request('GET', $url, [
                'query' => $requestQuery,
                'headers' => [
                    'Authorization' => 'sso-key ' . $this->godaddyKeysProvider->getAuthRow(),
                ],
            ]);
            $data = $this->prepareData($response);
            $domains = [];
            foreach ($data as $datum) {
                $domains[] = $datum['domain'];
            }
            return $domains;
        } catch (TransportExceptionInterface $e) {
            throw new GodaddyClientException($e->getMessage());
        }
    }

    /**
     * @param array $tlds
     * @param bool $privacy
     * @param bool|null $forTransfer
     * @param string|null $xMarketId
     * @return GoDaddyDomainAgreements[]
     * @throws GodaddyClientException
     */
    public function getDomainAgreements(
        array $tlds,
        bool $privacy,
        ?bool $forTransfer = null,
        ?string $xMarketId = null
    ): array
    {
        if (empty($tlds)) {
            return [];
        }
        $url = $this->godaddyUrlProvider->getUrl();
        $url .= '/v1/domains/agreements';
        $query = [
            'tlds' => $tlds,
            'privacy' => $privacy
        ];
        if (!is_null($forTransfer)) {
            $query['forTransfer'] = $forTransfer;
        }
        $headers = ['Authorization' => 'sso-key ' . $this->godaddyKeysProvider->getAuthRow()];
        if (!is_null($xMarketId)) {
            $headers['X-Market-Id'] = $xMarketId;
        }
        try {
            $response = $this->httpClient->request('GET', $url, [
                'query' => $query,
                'headers' => $headers,
            ]);
            $data = $this->prepareData($response);
            return GodaddyDomainAgreementsBuilder::butchBuild($data);
        } catch (TransportExceptionInterface $e) {
            throw new GodaddyClientException($e->getMessage());
        }
    }

    /**
     * @param array $body
     * @return GodaddyDomainPurchaseResponse
     * @throws GodaddyClientException
     */
    public function purchaseDomain(array $body): GodaddyDomainPurchaseResponse
    {
        $url = $this->godaddyUrlProvider->getUrl();
        $url .= '/v1/domains/purchase';
        try {
            $response = $this->httpClient->request('POST', $url, [
                'json' => $body,
                'headers' => ['Authorization' => 'sso-key ' . $this->godaddyKeysProvider->getAuthRow()],
            ]);
            $data = $this->prepareData($response);
            return GodaddyDomainPurchaseResponseBuilder::build($data);
        } catch (TransportExceptionInterface $e) {
            throw new GodaddyClientException($e->getMessage());
        }
    }

    /**
     * @param string $domain
     * @param GodaddyDnsRecord[] $records
     * @return bool
     * @throws GodaddyClientException
     */
    public function replaceDnsRecords(string $domain, array $records): bool
    {
        if (empty($records)) {
            return false;
        }
        $url = $this->godaddyUrlProvider->getUrl();
        $url .= "/v1/domains/$domain/records";
        $json = [];
        foreach ($records as $record) {
            $json[] = $record->jsonSerialize();
        }
        try {
            $response = $this->httpClient->request('PUT', $url, [
                'json' => $json,
                'headers' => ['Authorization' => 'sso-key ' . $this->godaddyKeysProvider->getAuthRow()],
            ]);
            return $response->getStatusCode() === Response::HTTP_OK;
        } catch (TransportExceptionInterface $e) {
            throw new GodaddyClientException($e->getMessage());
        }
    }
}