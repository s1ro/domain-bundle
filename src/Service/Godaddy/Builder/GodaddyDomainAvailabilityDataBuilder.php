<?php

namespace Domain\Service\Godaddy\Builder;

use Domain\Service\Godaddy\Model\GodaddyDomainAvailabilityData;

class GodaddyDomainAvailabilityDataBuilder
{
    /**
     * @param array $data
     * @return GodaddyDomainAvailabilityData
     */
    public static function build(array $data): GodaddyDomainAvailabilityData
    {
        return new GodaddyDomainAvailabilityData(
            $data['available'],
            $data['definitive'],
            $data['domain'],
            $data['period'] ?? null,
            $data['price'] ?? null,
            $data['currency'] ?? null
        );
    }
}