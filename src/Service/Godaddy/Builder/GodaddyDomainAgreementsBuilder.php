<?php

namespace Domain\Service\Godaddy\Builder;

use Domain\Service\Godaddy\Model\GoDaddyDomainAgreements;

class GodaddyDomainAgreementsBuilder
{
    /**
     * @param array $data
     * @return GoDaddyDomainAgreements
     */
    public static function build(array $data): GoDaddyDomainAgreements
    {
        return new GoDaddyDomainAgreements(
            $data['agreementKey'],
            $data['content'],
            $data['title'],
            $data['url'] ?? null
        );
    }

    /**
     * @param array $data
     * @return GoDaddyDomainAgreements[]
     */
    public static function butchBuild(array $data): array
    {
        $result = [];
        foreach ($data as $datum) {
            $result = self::build($datum);
        }
        return $result;
    }
}