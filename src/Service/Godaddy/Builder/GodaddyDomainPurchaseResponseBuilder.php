<?php

namespace Domain\Service\Godaddy\Builder;

use Domain\Service\Godaddy\Model\GodaddyDomainPurchaseResponse;

class GodaddyDomainPurchaseResponseBuilder
{
    /**
     * @param array $data
     * @return GodaddyDomainPurchaseResponse
     */
    public static function build(array $data): GodaddyDomainPurchaseResponse
    {
        return new GodaddyDomainPurchaseResponse(
            $data['itemCount'],
            $data['orderId'],
            $data['total'],
            $data['currency'] ?? null
        );
    }
}