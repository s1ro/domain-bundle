<?php

namespace Domain\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TestService
{
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(RequestStack $requestStack, SessionInterface $session)
    {
        $this->requestStack = $requestStack;
        $this->session = $session;
    }

    /**
     * @return bool
     */
    public function isTest(): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$request && !$this->session->get('domain_test')) {
            return false;
        }
        if (
            $request->headers->get('DOMAIN-TEST')
            ||
            $request->cookies->get('domain_test')
            ||
            $request->query->get('domain_test')
        ) {
            return true;
        }
        return false;
    }
}