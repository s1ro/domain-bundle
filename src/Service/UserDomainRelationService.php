<?php

namespace Domain\Service;

use Doctrine\ORM\EntityManagerInterface;
use Domain\Entity\UserDomainRelation;
use Domain\Repository\UserDomainRepository;

class UserDomainRelationService
{
    /**
     * @var UserDomainRepository
     */
    private $userDomainRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(UserDomainRepository $userDomainRepository, EntityManagerInterface $entityManager)
    {
        $this->userDomainRepository = $userDomainRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $userIdentifier
     * @return string[]
     */
    public function getUsersDomains(string $userIdentifier): array
    {
        $userDomainRelations = $this->userDomainRepository->findBy(['userIdentifier' => $userIdentifier]);
        $domains = [];
        foreach ($userDomainRelations as $userDomainRelation) {
            $domains[] = $userDomainRelation->getDomain();
        }
        return $domains;
    }

    /**
     * @param string $userIdentifier
     * @param string $domain
     * @return bool
     */
    public function addDomainToUser(string $userIdentifier, string $domain): bool
    {
        $userDomainRelation = new UserDomainRelation();
        $userDomainRelation
            ->setDomain($domain)
            ->setUserIdentifier($userIdentifier);
        $this->entityManager->persist($userDomainRelation);
        $this->entityManager->flush();
        return true;
    }
}