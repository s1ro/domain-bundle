<?php

namespace Domain\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Domain\Entity\UserDomainRelation;

/**
 * @method UserDomainRelation|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserDomainRelation|null findOneBy(array $criteria, ?array $orderBy = null)
 * @method UserDomainRelation[] findAll()
 * @method UserDomainRelation[] findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
class UserDomainRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserDomainRelation::class);
    }
}