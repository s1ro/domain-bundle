<?php

namespace Domain\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('domain');
        $rootNode = $treeBuilder->getRootNode();
        $rootNode
            ->children()
                ->arrayNode('godaddy')
                    ->children()
                        ->arrayNode('prod')
                            ->children()
                                ->variableNode('public_key')->end()
                                ->variableNode('private_key')->end()
                            ->end()
                        ->end()
                        ->arrayNode('dev')
                            ->children()
                                ->variableNode('public_key')->end()
                                ->variableNode('private_key')->end()
                            ->end()
                        ->end()
                        ->arrayNode('contact_info')
                            ->children()
                                ->arrayNode('addressMailing')
                                    ->children()
                                        ->variableNode('adress1')->defaultValue('')->end()
                                        ->variableNode('adress2')->defaultValue('')->end()
                                        ->variableNode('city')->defaultValue('')->end()
                                        ->variableNode('country')->defaultValue('')->end()
                                        ->variableNode('postalCode')->defaultValue('')->end()
                                        ->variableNode('state')->defaultValue('')->end()
                                    ->end()
                                ->end()
                                ->variableNode('email')->defaultValue('')->end()
                                ->variableNode('fax')->defaultValue('')->end()
                                ->variableNode('jobTitle')->defaultValue('')->end()
                                ->variableNode('nameFirst')->defaultValue('')->end()
                                ->variableNode('nameLast')->defaultValue('')->end()
                                ->variableNode('nameMiddle')->defaultValue('')->end()
                                ->variableNode('organization')->defaultValue('')->end()
                                ->variableNode('phone')->defaultValue('')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
        return $treeBuilder;
    }
}
