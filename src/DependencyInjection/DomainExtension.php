<?php

namespace Domain\DependencyInjection;

use Domain\Service\Godaddy\GodaddyContactInfoProvider;
use Domain\Service\Godaddy\GodaddyKeysProvider;
use Domain\Service\TestService;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class DomainExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

        $godaddyKeysProvider = new Definition(GodaddyKeysProvider::class);
        $godaddyKeysProvider
            ->addArgument($config['godaddy']['prod']['public_key'] ?? '')
            ->addArgument($config['godaddy']['prod']['private_key'] ?? '')
            ->addArgument($config['godaddy']['dev']['public_key'] ?? '')
            ->addArgument($config['godaddy']['dev']['private_key'] ?? '')
            ->addArgument($container->getDefinition(TestService::class));
        $container->setDefinition(GodaddyKeysProvider::class, $godaddyKeysProvider);

        $godaddyContactInfoProvider = new Definition(GodaddyContactInfoProvider::class);
        $godaddyContactInfoProvider->addArgument($config['godaddy']['contact_info']);
        $container->setDefinition(GodaddyContactInfoProvider::class, $godaddyContactInfoProvider);
    }
}